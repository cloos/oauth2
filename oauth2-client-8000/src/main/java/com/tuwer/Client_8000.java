package com.tuwer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author 土味儿
 * Date 2022/5/19
 * @version 1.0
 */
@SpringBootApplication
public class Client_8000 {
    public static void main(String[] args) {
        SpringApplication.run(Client_8000.class, args);
    }
}
